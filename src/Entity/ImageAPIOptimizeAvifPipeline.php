<?php

namespace Drupal\imageapi_optimize_avif\Entity;

use Drupal\imageapi_optimize\Entity\ImageAPIOptimizePipeline;
use Drupal\Core\File\FileSystemInterface;

/**
 * Wrap ImageAPIOptimizePipeline to copy AVIF derivative to proper directory.
 *
 * This wrapper allows for .avif image derivatives to be copied
 * to the correct directory after the AVIF image_api handler takes place.
 *
 * Class ImageAPIOptimizeAvifPipeline
 *
 * @package Drupal\imageapi_optimize_avif\Entity
 *
 * @param \Drupal\Core\File\FileSystemInterface $filesystem
 */
class ImageAPIOptimizeAvifPipeline extends ImageAPIOptimizePipeline {

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {
    parent::applyToImage($image_uri);
    // If the source file doesn't exist, return FALSE.
    $image = \Drupal::service('image.factory')->get($image_uri);
    if (!$image->isValid()) {
      return FALSE;
    }
    if (count($this->getProcessors())) {
      $avif_uri = $image_uri . '.avif';
      foreach ($this->temporaryFiles as $temp_image_uri) {
        $temp_avif_uri = $temp_image_uri . '.avif';
        if (file_exists($temp_avif_uri)) {
          $temp_image_uri = \Drupal::service('file_system')->copy($temp_avif_uri, $avif_uri, FileSystemInterface::EXISTS_RENAME);
          if ($temp_image_uri) {
            $this->temporaryFiles[] = $temp_avif_uri;
            break;
          }
        }
      }
    }
  }

}

