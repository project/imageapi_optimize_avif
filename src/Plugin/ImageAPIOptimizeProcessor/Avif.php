<?php

namespace Drupal\imageapi_optimize_avif\Plugin\ImageAPIOptimizeProcessor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorBase;

/**
 * AVIF Deriver.
 *
 * @ImageAPIOptimizeProcessor(
 *   id = "imageapi_optimize_avif",
 *   label = @Translation("AVIF Deriver"),
 *   description = @Translation("Clone image to AVIF")
 * )
 */
class Avif extends ConfigurableImageAPIOptimizeProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {
    $toolkit_id = $this->imageFactory->getToolkitId();

    // GD only works with PHP 8.1+.
    if ($toolkit_id === 'gd' && !function_exists('imageavif')) {
      return FALSE;
    }

    $source_image = $this->imageFactory->get($image_uri, $toolkit_id);

    if ($source_image) {
      $destination = $image_uri . '.avif';
      $quality = $this->configuration['quality'];

      // Check if ImageMagick supports avif.
      if ($toolkit_id == 'imagemagick' && in_array('avif', $this->imageFactory->getSupportedExtensions())) {
        try {
          $source_image->convert('avif');
          $source_image->setCompressionQuality($quality);
          $source_image->save($destination);

          // Fix issue where sometimes image fails to generate.
          if (filesize($destination) % 2 == 1) {
            file_put_contents($destination, "\0", FILE_APPEND);
          }

          return TRUE;
        }
        catch (\Exception $error) {
          // Something else went wrong, unrelated to the Tinify API.
          $this->logger->error('AVIF: Failed to optimize image using ImageMagick due to "%error".', ['%error' => $error->getMessage()]);
        }
      }
      // Go with GD.
      else {
        try {
          imageavif(
            $source_image->getToolkit()->getResource(),
            $destination,
            $quality
          );

          // Fix issue where sometimes image fails to generate.
          if (filesize($destination) % 2 == 1) {
            file_put_contents($destination, "\0", FILE_APPEND);
          }

          return TRUE;
        }
        catch (\Exception $error) {
          // Something else went wrong, unrelated to the Tinify API.
          $this->logger->error('AVIF: Failed to optimize image using GD due to "%error".', ['%error' => $error->getMessage()]);
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'quality' => 95,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo: Add ability to pick which image types allow derivatives.
    $form['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('Image quality'),
      '#description' => $this->t('Specify the image quality.'),
      '#default_value' => $this->configuration['quality'],
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 100,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['quality'] = $form_state->getValue('quality');
  }

}
