<?php

namespace Drupal\imageapi_optimize_avif\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Override 'image.style_public' controller to handle .avif deriver.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('image.style_public')) {
      $route->setDefault('_controller', 'Drupal\imageapi_optimize_avif\Controller\ImageStyleDownloadController::deliver');
    }
    if ($route = $collection->get('image.style_private')) {
      $route->setDefault('_controller', 'Drupal\imageapi_optimize_avif\Controller\ImageStyleDownloadController::deliver');
    }
  }

}
